from typing import List, Dict
from src.data.orkg_data.Strategy import Strategy
from orkg import ORKG
from requests.exceptions import ConnectionError
import time
import requests
import json
import os
from os.path import exists

from src.util.json_tools import read_json
from src.util.json_tools import write_json


class ORKGPyModule(Strategy):
    """
    Gets metadata of papers from the ORKG API.
    """

    def __init__(self):
        self.connector = ORKG(host="http://orkg.org/")
        self.predicate_url = 'http://orkg.org/api/statements/predicate/'
        self.subject_url = 'http://orkg.org/api/statements/subject/'

    def get_statement_by_predicate(self, predicate_id: str) -> Dict[str, List]:
        """
        Provides all paper ids and titles that have a research field.

        Parameters
        ----------
        predicate_id : str
            ID of "has research field"

        Returns
        -------
        Dict[str, list]
        """

        statement_data = {
            'complete': False,
            'last_page': -1,
            'paper': [],
            'label': [],
        }

        size = 50
        base_url = f'{self.predicate_url}{predicate_id}?size={size}'

        out_dir = 'data/orkg'
        os.makedirs(out_dir, exist_ok=True)
        filename = os.path.join(out_dir, f"predicate_{predicate_id}.json")

        if exists(filename):
            print('Reading checkpoint from', filename)
            statement_data = read_json(filename)

            if statement_data['complete']:
                return statement_data

        page = statement_data['last_page'] + 1

        # Download all predicate data
        while True:

            page_url = f'{base_url}&page={page}'
            print(page_url)

            try:
                response = requests.get(page_url)
            except ConnectionError:
                print(f'ConnectionError: on page {page_url}')
                print(f'Writing checkpoint to {filename}')
                write_json(filename, statement_data)
                exit(1)

            if response.ok:
                content = json.loads(response.content)['content']

                for statement in content:
                    statement_data['paper'].append(statement['subject']['id'])
                    statement_data['label'].append(statement['object']['label'])

                statement_data['last_page'] = page
                page += 1

                if len(content) < size:
                    statement_data['complete'] = True
                    break

        print(f'Write results to {filename}')
        write_json(filename, statement_data)

        return statement_data

    def get_statement_by_subject(self, paper_ids: List, meta_ids: Dict) -> Dict[str, list]:
        """
        Stores meta_infos for each paper in a Dict.
        Dict = {column_name: List[str], ...}

        Parameters
        ----------
        paper_ids : List[str]
            all paper_ids in orkg
        meta_ids : Dict
            relevant meta_ids (doi, ...)

        Returns
        -------
        Dict[str, list]
        """

        meta_infos = {key: [] for key in meta_ids.keys()}
        meta_infos['paper_ids'] = []

        for key, meta_id in meta_ids.items():
            meta_ids[key] = meta_id.split('/')[-1]

        out_dir = 'data/orkg'
        os.makedirs(out_dir, exist_ok=True)
        filename = os.path.join(out_dir, f"meta_infos.json")

        if exists(filename):
            print('Reading checkpoint from', filename)
            meta_infos = read_json(filename)

        # structure: {predicate_id: predicate_string}
        look_up = {v: k for k, v in meta_ids.items()}

        for inx, paper_id in enumerate(paper_ids):
            print(f"{paper_id} ({inx + 1}/{len(paper_ids)})")

            if (inx + 1) <= len(meta_infos['paper_ids']):
                print(f'Paper already in meta_infos: inx={inx} paper_id={paper_id}')
                continue

            try:
                response = self.connector.statements.get_by_subject(subject_id=paper_id, size=100, sort='id', desc=True)
            except ConnectionError:
                print(f'Connection error with paper {paper_id} --> restart program!')
                print(f'Writing checkpoint to {filename}')
                write_json(filename, meta_infos)
                exit(1)

            if response.succeeded:
                content = response.content
                infos = {key: [] for key in meta_ids.keys()}

                for statement in content:

                    pred_id = statement['predicate']['id']
                    if pred_id in meta_ids.values():
                        infos[look_up[pred_id]].append(statement['object']['label'])

                    if not infos['title']:
                        infos['title'].append(statement['subject']['label'])

                # build lists in meta info dict for every predicate field
                for iny, value in infos.items():
                    if len(value) == 0:
                        value = ""

                    if len(value) == 1:
                        value = value[0]

                    meta_infos[iny].append(value)

                meta_infos['paper_ids'].append(paper_id)

        # Save predicate data to file
        print('Write results to', filename)
        write_json(filename, meta_infos)

        return meta_infos
