import os
import json


# Read name.json data in directory
def read_json(filename: str):
    json_file = os.path.join(filename)
    with open(json_file) as f:
        data = json.load(f)

    return data


# Pretty print json data to console
def print_json(tag: str, data: any):
    print(tag, json.dumps(data, indent=2, sort_keys=True))


# Pretty print json data to file
def write_json(filename: str, data: any):
    with open(filename, "w") as file:
        json.dump(data, file, indent=2, sort_keys=True)
