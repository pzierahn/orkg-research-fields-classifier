import math
import random

import pandas as pd

import csv


def main(merged_data_path):
    """
    This function is the main function of the script.
    It splits the dataset into training and test data.
    :param merged_data_path: path to the dataset
    :return:
    """
    chunks = pd.read_csv(merged_data_path, chunksize=100000)
    distribution_per_chunk = []
    chunk_id = 0

    for chunk in chunks:
        print(chunk)
        this_distribution = get_total_sum(chunk)
        distribution_per_chunk.append(this_distribution)
        chunk_id += 1

    # for element in distribution_per_chunk:
    #     print(element)

    total_sum, summed_distribution = sum_distribution(distribution_per_chunk)

    # sample which paper to take
    sampled_numbers = get_sampled_numbers(total_sum, summed_distribution)

    # get sampled data
    reduced_df = get_reduced_data(sampled_numbers, merged_data_path)

    reduced_df.to_csv("data/processed/reduced_data.csv", index=False)

    # split training and test data
    training_df, test_df = split_training_test(reduced_df, total_sum)

    training_df.to_csv("data/processed/training_data.csv", index=False)
    test_df.to_csv("data/processed/test_data.csv", index=False)


def split_training_test(reduced_df, total_sum):
    """
    This function splits the reduced dataframe into training and test data. Currently we perform an 80/20 split.
    For the test data, we also only consider labels with more than 500 papers.
    :param reduced_df: dataframe containing the reduced data
    :param total_sum: dictionary containing the total number of articles per class
    :return: training and test dataframes
    """
    # define the split -> 80/20
    split_percentage = 0.8

    training_df = pd.DataFrame()
    test_df = pd.DataFrame()

    # create training and test distribution
    # distribution = {}
    for key, value in total_sum.items():
        print(key)
        print(value)
        # only labels with more than 500 papers should be considered for the test data
        if value > 500:
            # distribution[element] = [math.floor(total_sum[element]*0.8)+1, math.floor(total_sum[element]*0.2)]

            # get data frame for the current key and sample the training and test data
            label_df = reduced_df.query("label == '{}'".format(key))

            # if there used to be more than 5000 papers we need to adjust the value because the new number of papers hasn't been saved yet
            if value > 5000:
                value = 5000

            label_training_df = label_df.sample(n=math.floor(value*split_percentage)+1)
            label_test_df = label_df.drop(label_training_df.index)

            training_df = pd.concat([training_df, label_training_df])
            test_df = pd.concat([test_df, label_test_df])

        elif value >= 10:
            # distribution[element] = [total_sum[element], 0]

            label_df = reduced_df.query("label == '{}'".format(key))

            training_df = pd.concat([training_df, label_df])

    """training_df = pd.DataFrame()
    test_df = pd.DataFrame()
    for index, row in reduced_df.iterrows():
        label = row["label"]
        if label in distribution:
            print(index)
            if distribution[label][0] > 0:
                distribution[label][0] -= 1
                training_df = pd.concat([training_df, row])
            elif distribution[label][1] > 0:
                distribution[label][1] -= 1
                test_df = pd.concat([test_df, row])
            else:
                distribution.pop(label)"""

    print("split data")
    return training_df, test_df


def get_reduced_data(sampled_numbers, merged_data_path):
    """
    This function reduces the total data by only keeping the articles corresponding to the sampled numbers.
    :param sampled_numbers: dictionary containing the sampled numbers of papers per chunks per label
    :param merged_data_path: path to the dataframe containing the total data
    :return: reduced dataframe
    """
    chunky_df = pd.read_csv(merged_data_path, chunksize=100000)
    chunk_id = 0
    reduced_df = pd.DataFrame()
    for chunk in chunky_df:
        print(chunk)
        # if chunk_id == 0:
        #     reduced_df = pd.DataFrame(columns=list(chunk.columns))

        # testing faster version?
        for key, values in sampled_numbers.items():
            if values[chunk_id] > 0:
                print(key)
                print(values[chunk_id])
                print(chunk.query("label == '{}'".format(key)))
                try:
                    reduced_df = pd.concat([reduced_df, chunk.query("label == '{}'".format(key)).sample(n=values[chunk_id])])
                except ValueError:
                    print(chunk_id)
                    print(key)
                    print(values)

        # shuffle chunk to not always sample the same papers
        """chunk = chunk.sample(frac=1)
        chunk = chunk.reset_index(drop=True)

        for index, row in chunk.iterrows():
            print(index)
            label = row["label"]
            if label in sampled_numbers and sampled_numbers[label][0] <= summed_distribution[chunk_id][label]:
                reduced_df = pd.concat([reduced_df, row])
                del sampled_numbers[label][0]
                if len(sampled_numbers[label]) == 0:
                    sampled_numbers.pop(label, None)"""

        print(reduced_df)
        print("########################################")
        chunk_id += 1

    reduced_df = reduced_df.sample(frac=1)
    reduced_df = reduced_df.reset_index(drop=True)

    print(reduced_df)
    print("got reduced data")
    return reduced_df


def get_sampled_numbers(total_sum, summed_distribution):
    """
    This function randomly samples numbers per class and then randomly splits that number up into chunks containing articles with that label.
    Additionally, we only consider classes with at least 10 papers.
    If the number of papers per class is lower than max_train_threshold we don't need to sample and instead take every available article.
    We then split the sampled numbers per class into different chunks such that we randomly select different papers over all possible chunks.
    :param total_sum: dictionary containing the total number of articles per class
    :param summed_distribution: list of cumulative chunk distributions
    :return: dictionary containing the sampled numbers of papers per chunks per label
    """
    sampled_numbers = {}
    min_train_threshold = 10
    max_train_threshold = 5000

    for element in total_sum:
        # check if valid for training data
        if total_sum[element] >= min_train_threshold:
            numbers_list = []
            # check if down sampling necessary
            if total_sum[element] > max_train_threshold:
                numbers_range = list(range(1, total_sum[element]+1))
                for i in range(max_train_threshold):
                    numbers_list.append(numbers_range.pop(random.randint(0, len(numbers_range)-1)))
                    # numbers_list.append(random.randint(1, total_sum[element]))
            else:
                numbers_list = list(range(1, total_sum[element]+1))

            numbers_list.sort()
            sampled_numbers[element] = numbers_list

    print("got sampled numbers")
    # sampled_numbers = { "Physics": [1,3,5,7], "Computer Science": [1,4,6,8,93] }
    sampled_numbers_per_chunk = {}
    for key, values in sampled_numbers.items():
        print(key)
        print(values)
        # create a list with the length of number of chunks
        new_list = [0] * len(summed_distribution)
        for number in values:
            chunk_id = 0
            for chunk_dist in summed_distribution:
                if key in chunk_dist and number <= chunk_dist[key]:
                    new_list[chunk_id] += 1
                    break
                chunk_id += 1
        sampled_numbers_per_chunk[key] = new_list
    print(sampled_numbers_per_chunk["Physics"])
    print(sampled_numbers["Physics"])
    # sampled_numbers_per_chunk = { "Physics": [10,0,380,50], "Computer Science": [500,499,498,0,1] }
    return sampled_numbers_per_chunk


def sum_distribution(distribution):
    """
    This function sums up each chunk distribution to a final summed distribution.
    :param distribution: list of chunk distributions
    :return: Tuple of dictionary containing total count per class and list of cumulative chunk distributions
    """
    summed_distribution = []
    total_sum = {}
    for chunk_dist in distribution:
        for element in chunk_dist:
            if element in total_sum:
                total_sum[element] += chunk_dist[element]
                chunk_dist[element] = total_sum[element]
            else:
                total_sum[element] = chunk_dist[element]

        summed_distribution.append(chunk_dist)

    for chunk_dist in summed_distribution:
        print("#######################")
        for element in chunk_dist:
            print(str(chunk_dist[element]) + "/" + str(total_sum[element]))

    return total_sum, summed_distribution


def get_total_sum(df):
    """
    This function counts the papers per class in the dataframe.
    :param df: dataframe which needs counting
    :return: Dictionary containing the counts per class
    """
    this_distribution = {}

    for index, row in df.iterrows():
        label = row["label"]
        if label in this_distribution:
            this_distribution[label] += 1
        else:
            this_distribution[label] = 1
    return this_distribution


def get_count_per_class(path):
    """
    This function counts the papers per main class in the dataset.
    Here we only care about the top level classes of the taxonomy.
    :param path: path to the dataset
    :return:
    """
    df = pd.read_csv(path)
    df = df[["title", "abstract", "label"]]
    df["abstract"] = ["" if pd.isna(abstract) else abstract for abstract in df["abstract"]]
    df["text"] = [row["title"] + " " + (row["abstract"] or "") for index, row in df.iterrows()]

    counts = df.groupby('label')['text'].count()

    class Node:
        def __init__(self, name):
            self.name = name
            self.children = []

        def add_child(self, child):
            self.children.append(child)

    def construct_tree(file_path):
        root = Node('root')
        with open(file_path, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                parent = root
                for label in row:
                    if label:
                        child = Node(label.lower())
                        if child not in parent.children:
                            parent.add_child(child)
                        parent = child
        return root

    def find_parents(node, label):
        if node.name == label:
            return []
        for child in node.children:
            if child.name == label:
                return [node.name]
            path = find_parents(child, label)
            if path is not None:
                path.insert(0, node.name)
                return path
        return None

    tree = construct_tree('data/raw/mappings/taxonomy_new.csv')
    counting_dictionary = {"arts and humanities": 0, "engineering": 0, "life sciences": 0, "physical sciences & mathematics": 0, "social and behavioral sciences": 0}
    for label, count in counts.items():
        label = label.lower()
        if label in counting_dictionary.keys():
            counting_dictionary[label] += count
            continue
        parents = find_parents(tree, label)

        counting_dictionary[parents[1]] += count

    print(counting_dictionary)

    summe = 0
    for key, value in counting_dictionary.items():
        summe += value

    print(f"sum = {summe}")


if __name__ == "__main__":
    merged_data_path = "data/processed/arxiv_merge_preparation.csv"
    # reduced_data_path = "reduced_data.csv"
    # reduced_df = pd.read_csv(reduced_data_path)
    # total_sum = get_total_sum(reduced_df)
    # split_training_test(reduced_df, total_sum)
    main(merged_data_path)
    # get_count_per_class("training_data.csv")
